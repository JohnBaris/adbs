package dbms.memory;

import java.util.Arrays;

/**
 * Created by s1443483 on 12/03/18.
 */
public class BufferManager extends AbstractBufferManager{

    public BufferManager(Policy replPolicy, int numFrames, int pageSize, DiskSpaceManager spaceMan){
        super(replPolicy,numFrames, pageSize, spaceMan);
    }

    public int fetch(int pageAddr) throws FullBufferException{
        byte[] page;
        byte[] pageBuff;
//        switch (replPolicy){
//            case LRU: ;
//            case MRU: ;
//            case FIFO: ;
//            case LIFO: ;
//        }
        for (FrameInfo frame : this.bookkeeping) {
            if (frame.pageAddress == pageAddr) {
                frame.pinCount++;
                return frame.frameAddress;
            }
        }
        for (FrameInfo frame : this.bookkeeping) {
            if (frame.isEmptyFrame()) {
                page = this.spaceManager.read(pageAddr, pageSize);
                writeToBuffer(frame.frameAddress,page);
                frame.pageAddress = pageAddr;
                frame.pinCount++;
                return frame.frameAddress;
            }
        }
        for (FrameInfo frame : this.bookkeeping) {
            if (frame.pinCount == 0){
                if(frame.dirty){
                    pageBuff = readFromBuffer(frame.frameAddress, pageSize);
                    spaceManager.write(frame.pageAddress,pageBuff);
                }
                page = this.spaceManager.read(pageAddr, pageSize);
                writeToBuffer(frame.frameAddress,page);
                frame.pageAddress = pageAddr;
                frame.pinCount = 1;
                return frame.frameAddress;
            }
        }
        return 0;
    }

    public void release(int frameAddr, boolean modified) {
        byte[] page;
//        bookkeeping.get(frameAddr/pageSize);
//        for(FrameInfo frame : bookkeeping) {
//            if(frame.frameAddress == frameAddr) {
        if (modified) {
//            page = spaceManager.read(bookkeeping.get(frameAddr / pageSize).pageAddress, pageSize);
//            page = readFromBuffer(frameAddr, pageSize);
//            this.spaceManager.write(this.bookkeeping.get(frameAddr / pageSize).pageAddress, page);
            this.bookkeeping.get(frameAddr / pageSize).dirty = true;
//        }
//        this.bookkeeping.get(frameAddr/pageSize).pageAddress = -1;
            this.bookkeeping.get(frameAddr / pageSize).pinCount = 0;
        }
//            }
//        }
//    }
    }

    public byte[] readFromBuffer(int addr, int length) {
        return Arrays.copyOfRange(this.bufferPool, addr, addr+length);
    }

    public void writeToBuffer(int addr, byte[] page) {
        for ( int i=0; i < page.length; i++ ) {
            this.bufferPool[addr+i] = page[i];
        }
    }
}
